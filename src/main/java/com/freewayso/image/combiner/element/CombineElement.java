package com.freewayso.image.combiner.element;

import com.freewayso.image.combiner.enums.Direction;

/**
 * @Author zhaoqing.chen
 * @Date 2020/8/21
 * @Description 合并元素父类
 */
public abstract class CombineElement<T extends CombineElement> {
    private int x;                          //起始坐标x，相对左上角
    private int y;                          //起始坐标y，相对左上角
    private boolean center;                 //是否居中
    private Direction direction = Direction.LeftRight;    //绘制方向
    private float alpha = 1.0f;             //透明度
    private boolean repeat;                 //平铺
    private int repeatPaddingHorizontal;   //平铺水平间距
    private int repeatPaddingVertical;     //平铺垂直间距

    public int getX() {
        return x;
    }

    public T setX(int x) {
        this.x = x;
        return (T) this;
    }

    public int getY() {
        return y;
    }

    public T setY(int y) {
        this.y = y;
        return (T) this;
    }

    public boolean isCenter() {
        return center;
    }

    public T setCenter(boolean center) {
        this.center = center;
        return (T) this;
    }

    public Direction getDirection() {
        return direction;
    }

    public T setDirection(Direction direction) {
        this.direction = direction;
        return (T) this;
    }

    public float getAlpha() {
        return alpha;
    }

    public T setAlpha(float alpha) {
        this.alpha = alpha;
        return (T) this;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public T setRepeat(boolean repeat) {
        this.repeat = repeat;
        return (T) this;
    }

    public T setRepeat(boolean repeat, int repeatPadding) {
        this.repeat = repeat;
        this.repeatPaddingHorizontal = repeatPadding;
        this.repeatPaddingVertical = repeatPadding;
        return (T) this;
    }

    public T setRepeat(boolean repeat, int repeatPaddingHorizontal, int repeatPaddingVertical) {
        this.repeat = repeat;
        this.repeatPaddingHorizontal = repeatPaddingHorizontal;
        this.repeatPaddingVertical = repeatPaddingVertical;
        return (T) this;
    }

    public int getRepeatPaddingHorizontal() {
        return repeatPaddingHorizontal;
    }

    public int getRepeatPaddingVertical() {
        return repeatPaddingVertical;
    }
}
